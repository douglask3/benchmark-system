function data=plot_annual_average_over_range(data,syr,ryr,seasonal,rlat,rlon)

[a,b,c]=size(data);
if seasonal
    eyr=syr+c/12;
else
    eyr=syr+c;
end

if syr<=ryr(1) && eyr>=ryr(2)
    dc=[ryr(1)-syr  eyr-ryr(2)];
    dc
    if seasonal
        dc=dc*12;
        data=data*12;
    end
    dc(1)=dc(1)+1;
    dc(2)=c-dc(2);
    data=data(:,:,dc(1):dc(2));
    
    data=mean(data,3);
%     load mask2
%     plot_with_lims_dres_cline11(rlat,rlon,...
%         data,'average dimension',...
%         'colour',{'red'},...
%         'limits',[0.001 0.01 0.02 0.05 0.1 0.2],...
%         'line thickness',2.5,...
%         'boundry lines',landmask_vector,...
%         'xtick',[0],'ytick',[0],...
%         'turn off find missing values',...
%         'boundry line width',1,'axis',[100 160 -45 -10]) 
else
    data=zeros(a,b,'single');
end


         
    