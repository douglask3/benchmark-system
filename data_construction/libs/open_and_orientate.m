function data=open_and_orientate(filename,varname,globalt,flipt,rlat,rlon)
   data=open_netcdf_variable(0,filename,varname,0,0);
   if globalt
       data=remove_outside(data,rlat,rlon);
   end
   
   if flipt
       data=filp_data(data);
   end
end


