convert_annual_xyz_to_nc <-function()
  {
  
    filename="1999yfapar.csv"
    nyrs=8
    syr=1998
    a=720
    b=360
    
    filename_out='annual_fapar.nc'
    varname_out='fapar'
    
    data_out=array(NaN,dim=c(a,b,nyrs))
    data0=array(NaN,dim=c(a,b))
    
    for ( y in 1:nyrs) {
        substring(filename,1,4)=as.character(y+syr-1) 
        
        data_in=read.csv(filename,sep=",")
        
        lat=data_in[,1]
        lon=data_in[,2]
        
        j=2*(lat+89.75)+1
        i=2*(lon+179.75)+1
        
        data0[cbind(i,j)]=data_in[,3]
        data_out[,,y]=data0
    }
    
    data_out[which(data_out==-999)]=NaN
    print("yay1")
    nc=create.nc(filename_out,prefill=TRUE)
    dim.def.nc(nc, "lat", 360)
    dim.def.nc(nc, "lon", 720)
    dim.def.nc(nc, "time", nyrs)
print("yay2")
    ## Create two variables, one as coordinate variable
    var.def.nc(nc, "lat", "NC_FLOAT", "lat")
    var.def.nc(nc, "lon", "NC_FLOAT", "lon")
    var.def.nc(nc, "time", "NC_FLOAT", "time")
    var.def.nc(nc, varname_out, "NC_FLOAT", c("lon","lat","time"))
  print("yay3")  
    att.put.nc(nc, varname_out, "missing_value", "NC_FLOAT", -999)
print("yay4")    
    var.put.nc(nc, "lat", seq(-89.75,89.75,.5))
    var.put.nc(nc, "lon", seq(-179.75,179.75,.5))
    var.put.nc(nc, "time", 1:nyrs)
 print("yay5")   
    var.put.nc(nc, varname_out, array(NaN,dim=c(a,b,nyrs)))
     print("yay6")   
    var.put.nc(nc, varname_out, data_out)
    
    return(data_out)
  }