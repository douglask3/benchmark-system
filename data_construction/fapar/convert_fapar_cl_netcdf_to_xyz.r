convert_nc_xyz <-function() 
  {
    
    library("raster")
    library("ncdf")
    library("RNetCDF")
    
    nyrs=8
    
    ct_filename="clt_18512006.nc"
    ct_varname='clt'
    ct_start=c(1,1,(1998-1851)*12)
    ct_count=c(720,360,1)    
    
    sf_filename='../../bench_data/SeaWiFS_fapar_monthly.nc'
    sf_varname='fapar'
    sf_start=c(1,1,0)
    sf_count=c(720,360,1)
    
    filename_out='Outs/XeaWiFs_fapar_CRUTS3.1_cloud_2005-Dec.txt'    
    
    month_name=c('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec')
    
    xyz=read.csv("old_SeaWifS_fapar_2005-Dec.txt")
            
    j=round((xyz[,1]+89.75)*2+1)
    i=round((xyz[,2]+179.75)*2+1)
    
    ii=i+360
    ii[which(ii>720)]=ii[which(ii>720)]-720
    test=matrix(0,720,360)
    for ( y in 1:nyrs) {
        for (m in 1:12) {
            ct_start[3]=ct_start[3]+1
            sf_start[3]=sf_start[3]+1
            print(paste("year:",y+1997))
            print(paste("month: ",month_name[m]))
            #load up cloud
            print("    converting clouds")
            nc=open.nc(ct_filename)
            clouds=var.get.nc(nc,ct_varname,start=ct_start,count=ct_count)

            
            #load up fspar
            print("    converting fapar")
            nc=open.nc(sf_filename)
            seawifs=var.get.nc(nc,sf_varname,start=sf_start,count=sf_count)

            seawifs[which(seawifs==-999)]=NaN


            #load WHs old data
            test[cbind(i,j)]=clouds[cbind(ii,j)]
            image(test)
            xyz[,3]=i
            xyz[,4]=j
            # xyz[,3]=seawifs[cbind(i,j)]
            # xyz[,4]=clouds[cbind(ii,j)]
            
            substring(filename_out,35,38)=as.character(y+1997) 
            substring(filename_out,40,42)=month_name[m] 
            print(filename_out)
            write.table(xyz,filename_out,col.names=FALSE,row.names=FALSE,sep="  ")
            stop()
        }
    }    
  }
    
    
    
    
    
    
    
    