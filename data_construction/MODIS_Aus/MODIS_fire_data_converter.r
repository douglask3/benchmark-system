## set up path names, variable definitionsd and all that fancy stuff ##
configure <-function() {
    # load and source
    library("raster")
    library("ncdf")
    library("ncdf4")
    library("RNetCDF")
    source("../../libs/return_multiple_function_operator.r")
    source("../../libs/calculate_new_month_and_year.r")
    
    # input file info
    filename_in <<- 'data/dob_20020801_20020831.nc'
    varname_in <<- "julian_date_of_burn"
    varname_lat <<- "latitude"
    varname_lon <<- "longitude"
    yr_string_loc <<- c( 10,19)
    mn_string_loc <<- c(14,23)
    dy_string_loc <<- c(16,25)
    nmnths <<- 1
    syr <<- 2011
    smn <<- 8
    raster_size <<- c(17600,14000)
    raster_start <<- c(100,100)
    split_fact <<- 5
    agg_fact_range <<- c(100,100)
    
    # maske file info
    filename_mask <<- "data/Ausmask_0k.nc"
    varname_mask <<- "mask"
    
    # output file info
    filename_out <<- 'outputs/MODIS_Aus_burnt_fraction_2011_2012.nc'
    varname <<- 'mfire_frac'
    long_name <<- 'monthly burnt fraction'
    units_desc <<- 'fraction'
    date_descp <<- paste("date:", Sys.Date(), sep =" ")
    data_source <<- "MODIS Burnt Area Monthly Australia 250m V2.10 (V2.9/V2.5/V2.8) by Stefan W Maier at stefan.maier@cdu.edu.au; " 
    convert_descp <<- "Regridded from lcc high resolution, 0.0025 degree res grid to a 0.5 degree R by Doug Kelley"
    sript_name <<- "MODIS_fire_data_converter.r" #Doug 03/13: need to change so this is automated
    contact_descp <<- "See bitbucket.org/douglask3/benchmark-system for scripts and contact information"
    
    #ouput projection info
    lat_range <<- c(110.25,159.75,0.5)
    lon_range <<- c(-45.25,-10.25,0.5)
    projection_info <<- '+proj=longlat +datum=WGS84 +ellps=WGS84 +towgs84=0,0,0'
    
    #parameters
    ml <<- c(31,28,31,30,31,30,31,31,30,31,30,31)
    ml <<- c(31,28,31,30,31,30,31,31,30,31,30,31)
    
    #some initial set-p
    raster_size <<- raster_size-raster_start
    
}

## main function to be called from R terminal ##
MODIS_fire_data_converter <-function()
  {
    ## converts fire data from MODIS AUS at NT group from its super-high
    ## resolution to 0.5 degree grid.  
    configure()
    
    

    # set up grid from sample input fileready to transfer to tiff file
    # (small enough to open, but with no projection)
    set_up_raster_grid()
    
    
    # set up mask
    mask=raster(filename_mask,varname_mask)
    mask=resample(mask,req_grid)
    
    i=0
    j=1
    
    countp=floor(c(raster_size/split_fact,1))
    for (ncw in 1:(split_fact^2)) {
        i=i+1
        if (i>split_fact) {
            j=j+1
            i=1
        }     
        # setup starts for this loops ncdfs
        startp=find_starting_point_for_opening_next_lot_of_ncdfs(i,j)
        c(lat,lon):=set_up_lat_lon_vectors(startp,countp)
        print(startp)
        print(countp)
        year=syr
        mn=smn-1
    
        for (m in 1:nmnths) {
            c(mn, year):=calculate_new_month_and_year(mn,year)
            print(paste("looking at window ",ncw, " out of ", 
                split_fact^2,sep=""))
            #construct latest filename
            filename_in=
                change_filename_according_to_new_yr_and_mnth(mn,year)

            # open up, one split at a time
            nc=nc_open(filename_in)
            fdata=ncvar_get(nc,varid=varname_in,start=startp,count=countp)
            nc_close(nc)
            #fdata=apply(t(fdata),1,rev) 
            
            fdata=as.vector(is.na((fdata))==FALSE)            
            fdata=rasterFromXYZ(cbind(lon,lat,fdata),digits=8)
            
            agg_fact=calculate_aggrigation_factor(fdata)
            
            ndata=fdata
            ndata[,]=1
            ndata=aggregate(ndata,agg_fact,fun=sum)
            ndata=ndata/(agg_fact^2)
            #if (ncw==1) browser()
            fdata=aggregate(fdata,agg_fact)*ndata            
            fdata=extend(fdata,req_grid)
            fdata[is.na(fdata)]=0
            fdata=resample(fdata,req_grid)
            
            fdata[is.na(fdata)]=0
            
            fdata[fdata<0]=0
            fdata=fdata*mask
            out_raster[[m]]=out_raster[[m]]+fdata 
                
        }
        
    }
    # write in netcdf file
    writeRaster(out_raster,filename_out,format="CDF",overwrite=TRUE)
        
    nc <- open.nc(filename_out, write=TRUE)
    
    var.rename.nc(nc, 'variable', varname)    
    var.rename.nc(nc, 'value', 'time')
    
    att.put.nc(nc, varname, "long_name", "NC_CHAR", long_name)
    att.put.nc(nc, varname, "units", "NC_CHAR", units_desc)
    att.put.nc(nc, varname, "data source", "NC_CHAR", data_source)
    att.put.nc(nc, varname, "conversion information", "NC_CHAR", convert_descp)
    att.put.nc(nc, varname, "script name", "NC_CHAR", sript_name)
    att.put.nc(nc, varname, "more information", "NC_CHAR", contact_descp)
    att.put.nc(nc, varname, "Date created", "NC_CHAR", date_descp)  
	
    att.put.nc(nc, "time", "long_name", "NC_CHAR", "year") 
    att.put.nc(nc, "time", "description", "NC_CHAR", "years since 1970. 1 = 1970/71 fire season")    

	close.nc(nc)   
  }
   
## main programs dependancies ##
set_up_raster_grid <- function() {
    a <<- (lon_range[2]-lon_range[1])/lon_range[3]
    b <<- (lat_range[2]-lat_range[1])/lat_range[3]

    req_grid <<- raster(nrows=a,ncols=b,ymn=lon_range[1],
        ymx=lon_range[2],xmn=lat_range[1],xmx=lat_range[2])       
    projection(req_grid) <<- projection_info
        
    out_raster <<- brick(req_grid,nl=nmnths)    
    out_raster[,] <<- 0.0
}

find_starting_point_for_opening_next_lot_of_ncdfs <- 
    function(i,j) {
    startp=ceiling(c(raster_start[1]+1+(i-1)*raster_size[1]/split_fact,
            raster_start[1]+1+(j-1)*raster_size[2]/split_fact,1))
            
    return(startp)
}

set_up_lat_lon_vectors <- function(startp,countp) {   
    
    nc=nc_open(filename_in)
    
    vec=ncvar_get(nc,varid=varname_in,
        start=startp,count=countp)
    lat=ncvar_get(nc,varid=varname_lat,
        start=startp[2],count=countp[2])
    lon=ncvar_get(nc,varid=varname_lon,
        start=startp[1],count=countp[1])
    
    lat=signif(lat,7)    
    lon=signif(lon,8)    
    
    lat=seq(max(lat),min(lat),-0.0025)[1:length(lat)]
    lon=seq(min(lon),max(lon)+0.0025,0.0025)[1:length(lon)]
    
    vec[,]=0
    lat=as.vector(sweep(vec,2,lat,'+'))
    lon=as.vector(sweep(vec,1,lon,'+'))
    nc_close(nc)
    return(list(lat,lon))
}

change_filename_according_to_new_yr_and_mnth <- function(month,year) {
  
    if (round(year/4)==(year/4)) {
        ml[2]=ml[2]+1
    }
    
    for (loc in 1:2) { 
        substring(filename_in,yr_string_loc[loc],yr_string_loc[loc]+3)=
            as.character(year)
            
        filename_in=
            rename_month_part_of_string(filename_in,
            mn_string_loc[loc],month)            
    }  
    
    substring(filename_in,dy_string_loc[1],dy_string_loc[1]+1)='01'
    
    
    substring(filename_in,dy_string_loc[2],dy_string_loc[2]+1)=
            as.character(ml[month])
  
    print("opening and converting:")
    print(paste("   ",filename_in,spe=""))
    return(filename_in)
  
 }
 
calculate_aggrigation_factor <- function(fdata) {
            a1=cell_distance(xmin(fdata))
            a2=cell_distance(xmax(fdata))
            a3=cell_distance(ymin(fdata))
            a4=cell_distance(xmax(fdata))
            
            test=signif(c(a1,a2,a3,a4),3)
            
            agg_fact=1/min(test)                 
            
            if (agg_fact<agg_fact_range[1]) agg_fact=
                                                agg_fact_range[1]
            if (agg_fact>agg_fact_range[2]) agg_fact=
                                                agg_fact_range[2]
            
            return(agg_fact)
}
 
 cell_distance <-function(x) {
    x=x*2-0.5
    d=abs(round(x)-x)/2
    return(d)
}
    
  rename_month_part_of_string <- function(cstring,loc,mn)
  {
    if (mn<10) {
        substring(cstring,loc)='0'
        substring(cstring,loc+1)=as.character(mn)
    } else {
        substring(cstring,loc,loc+1)=as.character(mn)
    }   
   return(cstring)
}


##