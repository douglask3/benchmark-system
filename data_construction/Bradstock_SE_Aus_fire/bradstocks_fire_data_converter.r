configure <- function() {
    # load and source
    library("raster")
    library("ncdf")
    library("RNetCDF")
    
    # input file info
    grid_file <<- 'data/wf197071.jpeg'
    wfdata_file <<- 'data/wf197071.tif'    
    pbdata_file <<- 'data/pb197071.tif'
    mask_file <<- 'data/ibrasub.tiff'
    yr_string_loc <<- 8
    nyrs <<- 39
    syr <<- 1970
    agg_fact <<- 10
    
    # output file info
    filename_out_wf <<- 'outputs/Ross_group_data_wf_only.nc'
    filename_out_pb <<- 'outputs/Ross_group_data_pb_only.nc'
    filename_out <<- 'outputs/Ross_group_data.nc'
    filename_out_global <<- 'outputs/Ross_group_data_global_extent.nc'
    varname <<- 'afire_frac'
    long_name <<- 'annual burnt fraction'
    units_descp <<- 'fraction'
    date_descp <<- paste("date:", Sys.Date(), sep =" ")
    data_source <<- "Ross Bradstocks Group" 
    convert_descp_low <<- "Regridded from lcc high resolution, 100m grid to a 0.5 degree grid and converted to netcdf in R by Doug Kelley"
    sript_name <<- "bradstocks_fire_data_converter.r" #Doug 03/13: need to change so this is automated
    contact_descp <<- "See bitbucket.org/douglask3/benchmark-system for scripts and contact information"


    lat_range <<- c(110.25,159.75,0.5)
    lon_range <<- c(-45.25,-10.25,0.5)
    projection_info <<- '+proj=longlat +datum=WGS84 +ellps=WGS84 +towgs84=0,0,0'
}

bradstocks_fire_data_converter <-function()
  {
    ## converts fire data from Rosses group from its super-high
    ## resolution to 0.5 degree grid.
    configure()
    
    make_wf_and_pb_seperate_files(wfdata_file,filename_out_wf)
    make_wf_and_pb_seperate_files(pbdata_file,filename_out_pb)
    combine_pb_and_wf_fires()
    
  }

make_wf_and_pb_seperate_files <-function(filename_in,filename_outl) {
    
    # set up grid from jpeg file (to big to open) ready to transfer to tiff file
    # (small enough to open, but with no projection)
    tif_grid=raster(grid_file)
    proj0=projection(tif_grid)
    ext0=extent(tif_grid)
    
    # set up required grid
    a=(lon_range[2]-lon_range[1])/lon_range[3]
    b=(lat_range[2]-lat_range[1])/lat_range[3]
    
    
    int_grid=raster(nrows=nrow(tif_grid)/agg_fact,ncols=ncol(tif_grid)/agg_fact,
        ymn=lon_range[1],ymx=lon_range[2],
        xmn=lat_range[1],xmx=lat_range[2])


    req_grid=raster(nrows=a,ncols=b,ymn=lon_range[1],
        ymx=lon_range[2],xmn=lat_range[1],xmx=lat_range[2])
       
    projection(req_grid)=projection_info
        
    out_raster=brick(nrows=a,ncols=b,ymn=lon_range[1],
        ymx=lon_range[2],xmn=lat_range[1],xmx=lat_range[2],nl=nyrs)
    
    out_raster[,]=0.0

    projection(int_grid)=projection_info
    projection(out_raster)=projection_info
    
    rm(tif_grid)
    
    #set up mask
    mask=raster(mask_file)
    mask=aggregate(mask,fact=agg_fact)
    mask=projectRaster(mask,int_grid)
    mask=aggregate(mask,fact=10)
    mask=resample(mask,req_grid)
    mask=as.matrix(mask)
    mask[which(is.na(mask)==0)]=1

    
    year=syr-1
    
    for (y in 1:nyrs) {
    
        #Wildfire
    
        year=year+1
        substring(filename_in,yr_string_loc,yr_string_loc+3)=
            as.character(year)
        
        if (year+1<2000) {
            substring(filename_in,yr_string_loc+4,yr_string_loc+5)=
                as.character(year+1-1900)
        } else if (year+1>2009) {
            substring(filename_in,yr_string_loc+4,yr_string_loc+5)=
                as.character(year+1-2000)
        } else {
            substring(filename_in,yr_string_loc+4)='0'
            substring(filename_in,yr_string_loc+5)=
                as.character(year+1-2000)
        }
        print("opening and converting:")
        print(paste("   ",filename_in,spe=""))
        hdata=raster(filename_in)
        extent(hdata) <- ext0
        projection(hdata) <- proj0
        
        hdata=aggregate(hdata,fact=agg_fact)
        hdata=projectRaster(hdata,int_grid)
        hdata=aggregate(hdata,fact=10)

        temp=resample(hdata,req_grid)

        #convert from colour range 0-255 to fraction
        temp=(255-temp)/255
        
        #mask out areas not used
        temp=as.matrix(temp)*mask
        temp[which(temp<0)]=0        
        values(out_raster[[y]])=temp
        
        #pb
        substring(pbdata_file,yr_string_loc,yr_string_loc+3)=
            as.character(year)
        
        if (year+1<2000) {
            substring(pbdata_file,yr_string_loc+4,yr_string_loc+5)=
                as.character(year+1-1900)
        } else if (year+1>2009) {
            substring(pbdata_file,yr_string_loc+4,yr_string_loc+5)=
                as.character(year+1-2000)
        } else {
            substring(pbdata_file,yr_string_loc+4)='0'
            substring(pbdata_file,yr_string_loc+5)=
                as.character(year+1-2000)
        }
        print("opening and converting:")
        print(paste("   ",pbdata_file,spe=""))
        hdata=raster(pbdata_file)
        extent(hdata) <- ext0
        projection(hdata) <- proj0
        
        hdata=aggregate(hdata,fact=agg_fact)
        hdata=projectRaster(hdata,int_grid)
        hdata=aggregate(hdata,fact=10)

        temp=resample(hdata,req_grid)

        #convert from colour range 0-255 to fraction
        temp=(255-temp)/255
        
        #mask out areas not used
        temp=as.matrix(temp)*mask
        temp[which(temp<0)]=0   
        temp=temp+as.matrix(out_raster[[y]])
        values(out_raster[[y]])=temp
        
    }
    
    # write in netcdf file
    writeRaster(out_raster,filename_outl,format="CDF",overwrite=TRUE)
    assign_netcdf_attributes(filename_outl)
      
  }
    

combine_pb_and_wf_fires <-function() {
    configure()
    wf=brick(filename_out_wf,varname)
    pb=brick(filename_out_pb,varname)
    
    total=wf+pb
    
    global=brick(nrows=360,ncols=720,xmn=-180,
        xmx=180,ymn=-90,ymx=90,nl=nlayers(total))
    projection(global)= projection_info 
    global=resample(total,global)

    writeRaster(total,filename_out,format="CDF",overwrite=TRUE)
    assign_netcdf_attributes(filename_out)
    
    writeRaster(global,filename_out_global,format="CDF",overwrite=TRUE)
    assign_netcdf_attributes(filename_out_global)
}


assign_netcdf_attributes <- function(filename) {
    nc <- open.nc(filename, write=TRUE)
    
    var.rename.nc(nc, 'variable', varname)    
    var.rename.nc(nc, 'value', 'time')
    
    att.put.nc(nc, varname, "long_name", "NC_CHAR", long_name)
    att.put.nc(nc, varname, "units", "NC_CHAR", units_descp)
    att.put.nc(nc, varname, "data source", "NC_CHAR", data_source)
    att.put.nc(nc, varname, "conversion information", "NC_CHAR", convert_descp_low)
    att.put.nc(nc, varname, "script name", "NC_CHAR", sript_name)
    att.put.nc(nc, varname, "more information", "NC_CHAR", contact_descp)
    att.put.nc(nc, varname, "Date created", "NC_CHAR", date_descp)  
	
    att.put.nc(nc, "time", "long_name", "NC_CHAR", "year") 
    att.put.nc(nc, "time", "description", "NC_CHAR", "years since 1970. 1 = 1970/71 fire season")    

	close.nc(nc) 
}

