% %% compare fire datasets
close all
clear all
addpath libs/

%% comparison area
rlat=[-45.25 -10.75 0.5];
rlon=[110.75 159.75 0.5];

%% GFED 3 file info
filename_GFED3='../bench_data/fire_GFED.nc';
varname_GFED3='mfire_frac';
syr_GFED3=1996.5;
seasonal_GFED3=true;
global_GFED3=true;
flip_GFED3=false;

%% GFED 4 file info
filename_GFED4='GFED4/Outputs/Fire_GFEDv4_Burnt_fraction_0.5grid9.nc';
varname_GFED4='mfire_frac';
syr_GFED4=1995.5;
seasonal_GFED4=true;
global_GFED4=true;
flip_GFED4=false;

%% Bradstocks
filename_RossB='Bradstock_SE_Aus_fire/outputs/Ross_group_data.nc';
varname_RossB='afire_frac';
syr_RossB=1970.5;
seasonal_RossB=false;
global_RossB=false;
flip_RossB=true;

%% MODIS
filename_MODIS='MODIS_Aus/outputs/MODIS_Aus_burnt_fraction6.nc';
varname_MODIS='mfire_frac';
syr_MODIS=2002+7/12;
seasonal_MODIS=true;
global_MODIS=false;
flip_MODIS=true;

%% define plotting range for inter-annual
ryrs=[1970.5 2006];

%% define plotting range for annual average
ryrs=[1970.5 2006.5;
    1996  2006;
    1997 2006;
    2003 2004];

[nperiod,~]=size(ryrs);

%% open and orientate datasets

GFED3=open_and_orientate(filename_GFED3,varname_GFED3,...
    global_GFED3,flip_GFED3,rlat,rlon);
GFED4=open_and_orientate(filename_GFED4,varname_GFED4,...
    global_GFED4,flip_GFED4,rlat,rlon);
RossB=open_and_orientate(filename_RossB,varname_RossB,...
    global_RossB,flip_RossB,rlat,rlon);
MODIS=open_and_orientate(filename_MODIS,varname_MODIS,...
    global_MODIS,flip_MODIS,rlat,rlon);

[a b,~]=size(GFED3);

pdata=zeros(a,b,4,nperiod);

%% make annual average plot
for i=1:nperiod
    ii=nperiod-i+1;
    pdata(:,:,1,i)=plot_annual_average_over_range(GFED3,...
        syr_GFED3,ryrs(i,:),seasonal_GFED3,rlat,rlon);
    pdata(:,:,2,i)=plot_annual_average_over_range(GFED4,...
        syr_GFED4,ryrs(i,:),seasonal_GFED4,rlat,rlon);
    pdata(:,:,3,i)=plot_annual_average_over_range(RossB,...
        syr_RossB,ryrs(i,:),seasonal_RossB,rlat,rlon);
    pdata(:,:,4,i)=plot_annual_average_over_range(MODIS,...
        syr_MODIS,ryrs(i,:),seasonal_MODIS,rlat,rlon);
end

Ross_mask=(isinf(pdata(:,:,3,1))+isnan(pdata(:,:,2,4)))==1;
pdata(pdata<0)=0;
pdata(isnan(pdata))=0;

for i=1:nperiod
    temp=pdata(:,:,3,i);
    temp(Ross_mask)=NaN;
    pdata(:,:,3,i)=temp;
end
pdata=pdata*100;
    load mask2
    load aus_land_shape
    plot_with_lims_dres_cline11(rlat,rlon,...
        pdata,'same window',...
        'colour',{'red'},...
        'limits',[0.001 0.01 0.02 0.05 0.1 0.2]*100,...
        'line thickness',2.5,...
        'boundry lines',landmask_vector,...
        'xtick',[-180 180],'ytick',[-90 90],...
        'turn off find missing values',...
        'boundry line width',1,'axis',[120 160 -45 -10]) 

%% start with IAV plots


