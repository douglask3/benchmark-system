convert_monthly_to_annual <-function()
  {
    mobs=read.csv("../bench_data/runoff_Dai.csv",header=FALSE)  
    
    a=dim(mobs)
    
    nyrs=floor((a[2]-15)/12)

    aobs=matrix("NaN",a[1]-2,nyrs+15)

    #set up header
    aobs[1,1:15]=data.matrix(mobs[1,1:15])
   
    aobs[1,16:(nyrs+15)]=1990+1:(nyrs)

    
    aobs[2:(a[1]-2),1:15]=data.matrix(mobs[4:a[1],1:15])
    
    
    mdata=data.matrix(mobs[4:a[1],16:a[2]])

    mdata[which(mdata==-999)]=NaN
    
    b=dim(mdata)
    
    for (y in 1:nyrs) {
        sy=(y-1)*12+1
        ey=y*12
        

        aobs[2:(a[1]-2),15+y]=rowSums(mdata[,sy:ey])

    }
    
    
    write.csv(aobs,"../bench_data/annual_runoff_Dai.csv")
  }
    