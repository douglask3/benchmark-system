convert_and_regid_basin_raster <-function()
  {
  
    library("raster")
    library("ncdf")
    library("RNetCDF")
	
	filename="basin_info4.txt"
	filename_out="basin_num.nc"
	varname_out="basin_ID"
	long_name="basin ID number"
	date_descp=paste("date:", Sys.Date(), sep =" ")
	descp="Different basins mapped out using a unique number for each"
    data_source="GRDC basin map, piched from Anna" #CV
    convert_descp="Gridded to 0.5 degree res and converted to netcdf by Doug kelley"
    sript_name="convert_and_regid_basin_raster.r" #Doug 03/13: need to change so this is automated
    contact_descp="See bitbucket.org/douglask3/lpx2013_data_analysis for scripts and contact information"
 
	
	print("opening basin info")
	hres=raster(filename)
	
	print("regirdding")
	lgrid=raster(ncols=720,nrows=360)
	lres=resample(hres,lgrid)
	
	print("creating netcdf file")	
	writeRaster(lres,filename_out,format="CDF",overwrite=TRUE)
	
	print("adding ncdf attributes")
	nc <- open.nc(filename_out, write=TRUE)
    
    var.rename.nc(nc, 'basin_info4', varname_out)   
	
    att.put.nc(nc, varname_out, "long_name", "NC_CHAR", long_name)
	att.put.nc(nc, varname_out, "description", "NC_CHAR", descp)
    att.put.nc(nc, varname_out, "data source", "NC_CHAR", data_source)
    att.put.nc(nc, varname_out, "conversion information", "NC_CHAR", convert_descp)
    att.put.nc(nc, varname_out, "script name", "NC_CHAR", sript_name)
    att.put.nc(nc, varname_out, "more information", "NC_CHAR", contact_descp)
    att.put.nc(nc, varname_out, "Date created", "NC_CHAR", date_descp)  
	
    close.nc(nc)
  }
	