%% make GFED 4 data into ncdf
clear all;close all
%% set up input file paths and parametes
filename='data/GFED4.0_MQ_199506_BA.hdf';
month_file_loc=[21 22];
yr_file_loc=[17 20];
BF_varname='BurnedArea';
uncertain_filename='BurnedAreaUncertainty';
transpose=1;
flip=1;
nmonth=213;
syr=1995;
smn=06;

%% Set up output file info
% 0.5 degree
filename_lowres='Outputs/Fire_GFEDv4_Burnt_fraction_0.5grid-nc3.nc';
la=720;lb=360;ldlat=0.5;
convert_descp_lowres='read from HDF using matlab, regridded to 0.5 degree. By Doug Kelley';


% 0.25 degree
filename_hires='Fire_GFEDv4_Burnt_fraction_0.25grid6.nc';
convert_descp_hires='read from HDF using matlab by Doug Kelley';

% common to both
varnames={'mfire_frac','Uncert'};
date_descp=['date: ' date];
long_name={'burnt fraction','uncertainty in burnt fraction'};
start_date='Jun-1995';
units={'fraction','fraction'};
descp={'mothly burnt fraction','uncertainty estimates of burnt fraction'};
data_source='GFED version 4 monthlty data on 0.25 grid in HDF format. See www.globalfiredata.org/Data';

sript_name='data_construction/GFED4/convert_to_netcdf.m';
contact_descp='See bitbucket.org/douglask3/benchmarking_system for scripts and contact information';


%% find out more about the grid the dat is on
data=hdfread(filename,BF_varname);

if transpose==1
    data=data';
end

[a b]=size(data);
dlat=180/b;

clear data
%% set up new grid
%hdata=zeros(a,b,nmonth,2,'single');
ldata=zeros(la,lb,nmonth,2,'single');





%% loop through each file
m=smn-1;
y=syr;

for mn=1:nmonth
    mn
    m=m+1;
    if m>12
        m=1;
        y=y+1;
    end
    if m<10
        filename(month_file_loc(1))='0';
        filename(month_file_loc(2))=num2str(m);
    else
        filename(month_file_loc(1):month_file_loc(2))=num2str(m);
    end
    filename(yr_file_loc(1):yr_file_loc(2))=num2str(y);
    %% open file

    data=single(hdfread(filename,BF_varname));
    delt=single(hdfread(filename,uncertain_filename));
    
    if transpose==1
        hdatai(:,:,1)=data';
        hdatai(:,:,2)=delt';
    else
        hdatai(:,:,1)=data;        
        hdatai(:,:,2)=delt;
    end
    
    %% regrid and recale
    if flip==1
        lj=lb+0.5;
    else
        lj=0;
    end
    for j=1:b
        if flip==1
            fj=b-j+1;
            lj=lj-0.5;
            jj=ceil(lj);
        else
            fj=j;
            lj=lj+0.5;
            jj=ceil(lj);
        end
        
        li=0;
        for i=1:a
            li=li+0.5;
            ii=ceil(li); 
            for k=1:2
                ldata(ii,jj,mn,k)= ldata(ii,jj,mn,k)+hdatai(i,j,k);
            end
        end
        
        lat=-90+dlat/2+(j-1)*dlat;
        size_lat=lat_size(lat,dlat);
        %hdata(:,j,mn,:)=hdatai(:,fj,:)/(10000*size_lat);
        
        if lj~=jj
            lat=-90+ldlat/2+(jj-1)*ldlat;
            size_lat=lat_size(lat,ldlat);
            ldata(:,jj,mn,:)=ldata(:,jj,mn,:)/(10000*size_lat);
        end
    end   
    
end
    

    
%% make netcdf
% high res

% range_lon=[-180+dlat*0.5 180-dlat*0.5 dlat];
% range_lat=[-90+dlat*0.5 90-dlat*0.5 dlat];
% 
% nccreate(filename_hires,'lon','Dimensions',{'lon' a})
% ncwrite(filename_hires,'lon',range_lon(1):range_lon(3):range_lon(2))
% 
% nccreate(filename_hires,'lat','Dimensions',{'lat' b})
% ncwrite(filename_hires,'lat',range_lat(1):range_lat(3):range_lat(2))
% 
% nccreate(filename_hires,'time','Dimensions',{'month' nmonth})
% ncwrite(filename_hires,'time',1:nmonth)
% 
% for i=1:2
%     nccreate(filename_hires,varnames{1,i},'Dimensions',....
%         {'lon' a 'lat' b 'month' nmonth},'FillValue',-999)
%     ncwrite(filename_hires,varnames{1,i},hdata(:,:,:,i));
%     ncwriteatt(filename_hires,varnames{1,i},'long_name',long_name{1,i});   
%     ncwriteatt(filename_hires,varnames{1,i},'units',units{1,i}); 
%     ncwriteatt(filename_hires,varnames{1,i},'start_date',start_date);  
%     ncwriteatt(filename_hires,varnames{1,i},'description',descp{1,i});     
% end
% 
% ncwriteatt(filename_hires,'/','conversion',convert_descp_hires);
% ncwriteatt(filename_hires,'/','date',date_descp);
% ncwriteatt(filename_hires,'/','sript_name',sript_name);
% ncwriteatt(filename_hires,'/','data_source',data_source);
% ncwriteatt(filename_hires,'/','script',contact_descp);

%% low res
clear hdata

load mask2
landmask=landmask_0k';
landmask(find(landmask==-999))=nan;

for m=1:nmonth
    for k=1:2
        ldata(:,:,m,k)=ldata(:,:,m,k).*landmask;
    end
end


range_lon=[-180+ldlat*0.5 180-ldlat*0.5 ldlat];
range_lat=[-90+ldlat*0.5 90-ldlat*0.5 ldlat];

nccreate(filename_lowres,'lon','Dimensions',{'lon' la},'Format','classic')
ncwrite(filename_lowres,'lon',range_lon(1):range_lon(3):range_lon(2))

nccreate(filename_lowres,'lat','Dimensions',{'lat' lb})
ncwrite(filename_lowres,'lat',range_lat(1):range_lat(3):range_lat(2))

nccreate(filename_lowres,'time','Dimensions',{'month' nmonth})
ncwrite(filename_lowres,'time',1:nmonth)

for i=1:2
    nccreate(filename_lowres,varnames{1,i},'Dimensions',....
        {'lon' la 'lat' lb 'month' nmonth},'FillValue',single(-999),'Datatype','single')
    ncwrite(filename_lowres,varnames{1,i},ldata(:,:,:,i));
    ncwriteatt(filename_lowres,varnames{1,i},'long_name',long_name{1,i});   
    ncwriteatt(filename_lowres,varnames{1,i},'units',units{1,i});        
    ncwriteatt(filename_lowres,varnames{1,i},'start_date',start_date); 
    ncwriteatt(filename_lowres,varnames{1,i},'description',descp{1,i});     
end

ncwriteatt(filename_lowres,'/','conversion',convert_descp_lowres);
ncwriteatt(filename_lowres,'/','date',date_descp);
ncwriteatt(filename_lowres,'/','sript_name',sript_name);
ncwriteatt(filename_lowres,'/','data_source',data_source);
ncwriteatt(filename_lowres,'/','script',contact_descp);
    

