compare_fire_dataset <- function()
  {
  
    library("raster")
    library("ncdf")
    library("RNetCDF")
    
    
    # GFED3 inputs info
    file_GFED3='../bench_data/fire_??.nc'
    varname=GFED3='mfire_frac'
    GFED3_lat_range
    
    # GFED4 input info
    file_GFED4='GFED4/Outputs/Fire_GFEDv4_Burnt_fraction_0.5grid.nc'
    varname_GFED4='mfire_frac'
    
    # Bradstock
    file_RS='Bradstock_SE_Aus_fire/Ouputs/??.nc'
    varname_RS='afire_frac'
    
    #
    GFED3=raster(file_GFED3,varname_GFED3)
    GFED4=raster(file_GFED4,varname_GFED4)
    RS=raster(file_RS,varname_RS)
    
    GFED4=resample(GFED4,GFED3)
    RS=resample(RS,GFED3)
    
       

    