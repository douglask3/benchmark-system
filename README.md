Metric codes are in R. R is a free software environment for statistical computing, which can be used on windows, Mac OS or Unix.
To download and install http://www.r-project.org/
There are some extra packages you need. Read the more detailed readme for info.

any questions or comments, email douglas.kelley@students.mq.edu.au

If using, please cite Kelley et al. 2013