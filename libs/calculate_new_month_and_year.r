calculate_new_month_and_year <- function(month,year,base=12) {
    month=month+1
    if (month>base) {
        month=1
        year=year+1
    }
    return(list(month,year))
}