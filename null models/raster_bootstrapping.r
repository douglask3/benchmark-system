raster_bootstrapping <- function(dlat)
  {
    print("************************************")
    print("Starting bootstrapping")
    print("------------------------------------")
    ntest=100;
    print("loading metrics")
    #source("benchmark_cfg.R")
    source("../metrics/NME_streamlined.R")    
    #cfgs=benchmark_cfg()
    
    print("loading observations")
    ncfile=open.ncdf("../bench_data/annual_fapar.nc")
    obs=get.var.ncdf(ncfile,"fapar")
    #write.csv(obsi,"obsi.csv")
    a=dim(obs)
	
	obsi=matrix(0,a[1]*a[2],1)
	size_lat=obsi
	
	p=0;
	
	print("rearranging in xyz")
	for (i in 1:a[1]) {
		for (j in 1:a[2]) {
			if (is.na(obs[i,j,1])==0 && obs[i,j,1]>=0) {
				p=p+1
				obsi[p,1]=sum(obs[i,j,])/12
				size_lat[p,1]=lat_size(-89.75+(j-1)*0.5,0.5)
			}
		}
	}
	
	remove(obs)

	obs=obsi[1:p,1]
	
	print("performing bootstrapping")
	obsr=obs;
	scores=array(0,dim=c(3,2,ntest))
	for (tn in 1:ntest) {
		print(tn)
		for (i in 1:p) {
			r=ceiling(runif(1,0,p))
			obsr[i]=obs[r]
		}
		#write.csv(obsr,"obsr.csv")
		#write.csv(obs,"obs.csv")
		#stop()
		scores[,,tn]=NME_streamlined(obsr,obs,size_lat)
	}
	
	

#	scores=array(0,dim=c(3,2,ntest))
#	obsr=matrix(0,a[1],a[2])
#	
#    threshold0=10
#    threshold=threshold0
#    print(paste(0,"%",sep=""))
#	for (t in 1:ntest) {
#		for (i in 1:a[1]) { #lon
#			if (100*i/a[1]>threshold) {
#				print(paste(threshold,"%",sep=""))
#				threshold=threshold+threshold0
#			}
#			for (j in 1:a[2]) {
#				if (is.na(obs[i,j])==0) {
#					r1=ceiling(runif(1,0,a[1]))
#					r2=ceiling(runif(1,0,a[2]))
#					
#					while (is.na(obs[r1,r2])) {
#						r1=ceiling(runif(1,0,a[1]))
#						r2=ceiling(runif(1,0,a[2]))
#					}
#					obsr[i,j]=obs[r1,r2]
#				}
#			}
#		}
#		
#		scores[,,t]=NME(obsr,obs,2,FALSE)
#	}
	    
    return(scores)
    
    
  }
    
    
    
          
          
          
      
      
    
    
    