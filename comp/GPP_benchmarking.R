GPP_benchmarking <- function(mod,test_uncert,dlat)
  {
    print("************************************")
    print("Starting Gross Primary Production (GPP) benchmarking")
    print("------------------------------------") 
    print("GPP: loading metrics and configuartion")
    source("benchmark_cfg.R")
    source("metrics/NME.R")  
    
    cfgs=benchmark_cfg()
    
    print("GPP: loading observations")
	print(cfgs$GPP_file)
    obsi=read.csv(cfgs$GPP_file,header=TRUE) 
    a=dim(obsi)   
	
    obs=data.matrix(obsi)   
    a=dim(obs)

    obsi=array(0,dim=c(a[1],6))    
    obsi[1,1:2]=obs[1,1:2]

    p=1    
    for (i in 2:a[1]) {
    
        if (obs[i,1]<cfgs$lat_range[1] || obs[i,1]>cfgs$lat_range[2] ||
            obs[i,2]<cfgs$lon_range[1] || obs[i,2]>cfgs$lon_range[2]) {
            next
        }
      if (floor((obs[i-1,1]/dlat)+0.5)==floor((obs[i,1]/dlat)+0.5) &&
          floor((obs[i-1,2]/dlat)+0.5)==floor((obs[i,2]/dlat)+0.5)) {
        obsi[p,3]=obsi[p,3]+obs[i,3]
        obsi[p,4]=obsi[p,4]+1
		if (test_uncert) {
				obsi[p,5]=obsi[p,5]+max(0,obs[i,3]-obs[i,4])
				obsi[p,6]=obsi[p,6]+obs[i,3]+obs[i,4]
			}
      } else {
        p=p+1
        obsi[p,1:2]=obs[i,1:2]
        obsi[p,3]=obs[i,3]
        obsi[p,4]=1
		if (test_uncert) {
				obsi[p,5]=max(0,obs[i,3]-obs[i,4])
				obsi[p,6]=obs[i,3]+obs[i,4]
		}
      }
    }

    for (i in 1:p) {
      obsi[i,3]=obsi[i,3]/obsi[i,4]
		if (test_uncert) {
			obsi[i,5:6]=obsi[i,5:6]/obsi[i,4]
		}
    }

    remove(obs)
    print(obsi)
    dat=obsi[1:p,]    
    dat[,4]=0
    dev.new(); image(mod[,,1,1])
    remove(obsi)
    a=dim(mod)
    tt=array(NaN,dim=c(720,360))
    for (i in 1:p) {
      jj=round(2*(dat[i,1]+89.75)+1);
      ii=round(2*(dat[i,2]+179.75)+1);
      dat[i,4]=sum(mod[ii,jj,,])/a[4]
      tt[ii,jj]=1
    }
    #write.csv(dat,"dat_LPX.csv")
    
    remove(mod)
    print("Performing NME comparisons")
     
    mod=matrix(0,p,1)
    obs=matrix(0,p,1)
	obs_max=matrix(0,p,1)
	obs_min=matrix(0,p,1)
    
    mod[,1]=dat[,4]    
    obs[,1]=dat[,3]
	obs_max[,1]=dat[,6]
	obs_min[,1]=dat[,5]
    
    mod[which(mod<0)]=NaN
    #write.csv(cbind(mod,obs),"Siimple.csv")
    mod=mod*2
    remove(dat)
    score=NME(mod,obs,0,TRUE)
    print("Scores obtained for NME & NMSE - step 1,2 & 3")
    print(score)
    
    plot(obs,mod)
    
	if (test_uncert) {
        print("Performing NME uncertainty comparisons")
        score_max=NME_uncert(mod,obs,obs_min,obs_max,0)
        score_min=NME_uncert(mod,obs,obs_min,obs_max,0)
        print("Scores for the uncertainty obtained for NME & NMSE - step 1,2 & 3")
        print(score_max)
    
    }
    
    remove(dat)
    
	plot(mod,obs,cex=1.5,col="black",pch=4,xaxs='i',yaxs='i',
		xlim=c(0, 4000),ylim=c(0, 3000))
	
    return(score)
    
    
  }
  
    