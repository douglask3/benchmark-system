fire_benchmark <- function(mod)
  {
    pp=0
    print("************************************")
    print("Starting Fire benchmarking")
    print("------------------------------------")
    
    print("Fire: loading libraries and metrics and configuration")
    library(ncdf)
    source("benchmark_cfg.R")
    source("metrics/seasonal.R")
    source("metrics/NME.R")
	source("metrics/NME_uncert.R")
    source("libs/dim_check3.R")
    source("libs/lat_size.R")
	source("libs/find_uncert_points.R")
    
    source("libs/remove_outside_range.R")
	cfgs=benchmark_cfg()
	gc()	#clear image of things clogging up memory
	

    print("Fire: loading observations")
	fire_file=open.ncdf(cfgs$fire_file)
	a=fire_file$dim$lon$len
	b=fire_file$dim$lat$len
	start_point=(cfgs$fire_start_year-cfgs$fire_file_start)*12-1
	count_point=(cfgs$fire_no_years)*12
	
    obs=get.var.ncdf(fire_file,cfgs$fire_varname,
		start=c(1,1,start_point),count=c(a,b,count_point))
	
    obs=remove_outside_range(obs)
    
    print("Fire: loading observational uncertainty")
    fire_file=open.ncdf(cfgs$fire_file)
    obs_var=get.var.ncdf(fire_file,cfgs$fire_uncertainty_varname,
		start=c(1,1,start_point),count=c(a,b,count_point))
    obs_var=remove_outside_range(obs_var)
    
    ai=dim(obs)  #check size of observation matrix
    
    #a=no. lon cells; b=no. lat cells; c=months; d=years
    a=ai[1]; b=ai[2]; c=12; nyrs=cfgs$fire_no_years
    dlat=180/ai[2]
    
    ai=dim_check3(mod,obs)
    a=ai$a    

    # calculate observational mean, max and min annual average    
    aaobs=rowSums(obs,dims=2)/nyrs
    aaobs_max=aaobs+rowSums(obs_var,dims=2)/nyrs
    
    obs_var=obs-obs_var
    obs_var[which(obs_var<0)]=0
    aaobs_min=rowSums(obs_var,dims=2)/nyrs

    
    rm(obs_var)
 
    # calculate observational inter annual 
    lat=180/a[2]
    lat=-90+((1:a[2])-1)*dlat+dlat/2
    lat=lat_size(lat,dlat)
    
    
    mnobs=colSums(sweep(obs,MARGIN=2,lat,'*'),dims=2,na.rm=TRUE)   
    yr_index=as.vector(
        t(matrix(1:nyrs,nyrs,12)))
    
    anobs=matrix(0,1,nyrs)
    for (y in 1:nyrs) {
        anobs[1,y]=sum(mnobs[which(yr_index==y)],na.rm=TRUE)
    }

    
    # calculate modelled annual mean and inter annual 
    anmod=matrix(0,1,nyrs)
	aamod=rowSums(mod,dims=2,na.rm=TRUE)/nyrs #annual average modelled
    anmodi=colSums(sweep(mod,MARGIN=2,lat,'*'),dims=2,na.rm=TRUE)               #annual global modlled   
    anmod[]=colSums(anmodi,na.rm=TRUE)
    
    if (ai$seasonal) {
        # calculate  monthly vales for seasonality comparisons
        mobs=array(0,dim=a[1:3])
        mn_index=as.vector(matrix(1:12,nyrs,12))
        for (m in 1:12) {
            mobs[,,m]=rowSums(obs[,,which(mn_index==m)],dim=2)
        }       
        
        mmod=array(0,dim=a[1:3])
        mmod=rowSums(mod,dim=3)
        
    }
    rm(obs,mod)

    #Do the stats
    print("------------------------------------")
    print("Fire: performing statistics")
    print("Fire: performing annual average statistics") 

    aa=NME(aamod,aaobs,2,TRUE)
    #print("Fire: Assessing ucertainty")
    #print("range of annual average metric results") 
    #aa_unc=NME_uncert(aamod,aaobs,aaobs_min,aaobs_max,2)
	
	print("mean")
	print(aa)
	#print("unc")
	#print(aa_unc)

    remove(aamod,aaobs,aaobs_max,aaobs_min)

    if (ai$seasonal) {
      print("Fire: performing seasonal statistics")      
      ss=seasonal(mmod,mobs,2)
      remove(mmod,mobs)
      print("Seasonal")
      print(ss)
    }

    
    print("Fire: performing IAV statistics")
    ia=NME(anmod,anobs,0,TRUE)
    remove(anobs,anmod,anobs_max,anobs_min)
    print(ia)
    print("------------------------------------")
      
    
    list(seasonal=ss,#seasonal_max_value=ss_max,seasonal_min_value=ss_min,
      annual_average=aa,annual_average_max_value=aa_unc,
        annual_average_min_value=aa_min,
      inter_annual=ia, inter_annual_max_value=ia_max,
        inter_annual_min_value=ia_min)
  }