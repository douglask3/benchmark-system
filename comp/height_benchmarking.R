height_benchmarking <- function(mod)
  {
    pp=0
    print("************************************")
    print("Starting height benchmarking")
    print("------------------------------------")
    
    print("height: loading libraries and metrics")
    library(ncdf)
    source("benchmark_cfg.R")
    source("metrics/NME.R")    
    source("libs/dim_check.R")
    source("libs/dim_check2.R")
    source("libs/remove_outside_range.R")
    
    print("height: loading configurations")    
    cfgs=benchmark_cfg()
    
    print("height: loading observations")
    height_file=open.ncdf(cfgs$height_file)
    obs=get.var.ncdf(height_file,cfgs$height_varname)
    
    obs=remove_outside_range(obs)
    
    dim_check(mod,obs)

    #Do the stats
    print("------------------------------------")
    print("height: performing statistics")

    score=NME(mod,obs,2,TRUE)
    remove(mod,obs)
    
    print("------------------------------------")  

    
    return(score)
  }