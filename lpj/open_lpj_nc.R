open_lpj_nc <- function(file_name,varname,y1850_file_no,
    recentre,startyr,nyrs)
  {
    library(ncdf)
    
    start_yr=y1850_file_no+(startyr-1850)

    print("************************************")
    filename=paste(file_name,start_yr,".nc",sep="")
    print("Opening:")
    print(varname)
    print("in:")
    print(filename)
    ncfile=open.ncdf(filename)
    mod1=get.var.ncdf(ncfile,varname)
    print("------------------------------------")
    
    a=dim(mod1)
    
    b=length(a)
    
    if (b==3) {
      mod =array(0,dim=c(a[1],a[2],a[3],nyrs))
      modi=array(0,dim=a)
    } else {
      mod =array(0,dim=c(a[1],a[2],1,nyrs))
      modi=array(0,dim=c(a[1],a[2],1))
      a[3]=1
    }
    
    if (recentre) {
        if (b==3) {
        
          mod[1:(a[1]/2),,,1]=mod1[(1+a[1]/2):a[1],,]
          mod[(1+a[1]/2):a[1],,,1]=mod1[1:(a[1]/2),,]
          
        } else {
        
          mod[1:(a[1]/2),,,1]=mod1[(1+a[1]/2):a[1],]
          mod[(1+a[1]/2):a[1],,,1]=mod1[1:(a[1]/2),]
          
        }
      } else {
        mod[,,,1]=mod1
      }
    p=1
    while (p<nyrs) {
    
      y=p+start_yr
      p=p+1      
      remove(filename)
      filename=paste(file_name,y,".nc",sep="")
      print(y)
      print("Opening:")
      print(varname)
      print("in:")
      print(filename)
      print("------------------------------------")
      
      ncfile=open.ncdf(filename)
      modi[,,]=get.var.ncdf(ncfile,varname)
      if (recentre) {
        mod[1:(a[1]/2),,,p]=modi[(1+a[1]/2):a[1],,]
        mod[(1+a[1]/2):a[1],,,p]=modi[1:(a[1]/2),,]
      } else {
        mod[,,,p]=modi
      }
    }
	
	missing_value_loc=which(mod>1E9)
	mod[missing_value_loc]=NaN
           
    print("************************************")
    return(mod)
    
  }
    
    