Add: steps to make R run faster
http://yusung.blogspot.com.au/2008/06/speed-up-r-make-r-run-faster.html

The system is coded in R is a free software environment for statistical computing, which can be used on windows, Mac OS or Unix.
To download and install http://www.r-project.org/

The system is split into three sections. The metrics (in directory metrics); the comparison codes (in the directory comp) which also open and arrange the benchmark data (in bench_data); and opening and arranging model outputs (veg_mod). These are combined together using benchmark_main.R, which is driven by information provided by benchmark_cfg.R and benchmark_xxx_cfg.R where xxx is the relevant vegetation model. If your veg model is already supported by this system, you only need look up information about how to run the system in part 1 below. If you want to make more changes to the system, use different benchmark data, or use the metric by themselves, have a look at the rest. Info on adding different veg models will be added later.

1. Set up the current system
1. Download and install R(easy to do, files and instructions can be found at http://www.r-project.org/)
2. Install netcdf package (Packages menu -> Install Package -> <select country> -> <selected ncdf> -> <click okay>.
3. Download and unzip system files, available from http://bio.mq.edu.au/bcd/benchmarks/
4. Run R
5. change directory (using �setwd(<path>)� ) to the Benchmark_system directory created when you unzipped the system files
6. Type �source(�benchmark_main.R�)

The system is now ready to run. However, before you can run it, you will need to make sure that the configuration files are properly set in the two sections below.

Benchmark_cfg.R
The configuration file stores information about which  comparisons are to be made, benchmark dataset, and time periods the comparison are to be conducted over. Assuming you are using the same datasets and do not change the directory structure, you need to change the following:

1. In the first section, change the comparisons you wish to make to �TRUE� and the ones you wish to skip as �FALSE�
2. Under each section, you may want to change the start and number of years you wish to do the comparison for. You will only be able to set this to years where there is benchmark data (see Kelley et atl attached) and years where you have model output.
3. Change your model type to one of the following:
	�LPJ_cpp�: for the lpj/lpx family of models that uses the c++ I/O driver, outputting files in netcdf format.

You can also change the benchmark datasets you use, but if the format is different, you will need to edit its respective comparison file

Benchmark_xxx_cfg.R
xxx stands for model (and output type) versions. If your model is not listed below, then it is not yet supported. If you end up writing configuration and code for your model, please share it with everyone else (contact douglas.kelley@students.mq.edu.au)

Benchmark_lpj_cfg.R
Configuration file for lpj/lpx family of models that uses the c++ I/O driver, outputting files in netcdf format.

1. Change filenames for the variables you wish to benchmark
2. Specify the number of runs years up to 1850 for this variables output file. Using standard spin-up protocol using CRU data, this will be the equivalent to the number of spin-up years. However, if you spun-up to another point you�ll have to adapt this number. i.e., spinning up to 1900 means take 50  (1900-1850) of the number of spin up years.
3. Specify if the grid needs to be re-centring by setting �recentre_grid� to TRUE or FALSE. The grid needs re- centring if it is pacific centric.


That�s all you need for running the system as is. Below is more info about the metrics and comparisons:

Metrics
The individual metrics are coded to be easily used separately from the system. Each take inputs either as gridded (i.e. 2D matrix) or listed (i.e. vector), although if area weighting is required, they�ll only take gridded at the moment which is assumed to be global with cells evenly spaced along the latitude axis.

The usage is score=MMMM(modelled,observed,latitude dimension)

Modelled & observed: missing values must be assigned NA or NaN
Latitude dimension: set to 0 if no area weighting is required. Set to 1 if latitude varies along the x axis; and 2 if along the y axis of the matrix.
MMMM: replace with �NME� for Normalised Mean Error and Normalised Mean Squared Error comparisons, for all three steps specified in Kelley et al (submitted); �MM� for Manhattan Metric and Square Chord Distance; �seasonal� for Mean Phase Distance & NME/NMSE comparisons of season concentration.

More in Kelley et al (submitted)


Comparisons:
Not all comparisons are up and running yet. The following have been tested and are okay:

Fire
Fapar
vegetation cover
Net Primary Production (NPP)
Gross Primary Production (GPP)
height 




Other will be coming online over the next few weeks





If your using this in a study, please cite Kelley et al (submitted)

Any questions or comments, email douglas.kelley@students.mq.edu.au
